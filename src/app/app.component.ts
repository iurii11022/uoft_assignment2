import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public title = 'assignment 2';
  public oddNumbers: number[] = [];
  public evenNumbers: number[] = [];

  public onIntervalFired (newNumber: number) {
    newNumber % 2 === 0
      ? this.evenNumbers.push(newNumber)
      : this.oddNumbers.push(newNumber)
  }
}
