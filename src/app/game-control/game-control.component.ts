import {Component, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'app-game-control',
  templateUrl: './game-control.component.html',
  styleUrls: ['./game-control.component.scss']
})
export class GameControlComponent {
  private interval!: number;
  private lastNumber: number = 0;

  public isGameStarted = false;

  @Output()
  public intervalFired = new EventEmitter<number>()


  public onStartGame() {
    this.isGameStarted = true

    this.interval = setInterval(()=>{
      this.lastNumber++;
      this.intervalFired.emit(this.lastNumber);
    },1000)
  }

  public onPauseGame() {
    clearInterval(this.interval);
    this.isGameStarted = false
  }
}
